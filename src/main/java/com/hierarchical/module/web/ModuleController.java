package com.hierarchical.module.web;

import com.hierarchical.module.dto.ModuleRequest;
import com.hierarchical.module.entiy.Module;
import com.hierarchical.module.service.ModuleService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@AllArgsConstructor
@RequestMapping("/modules")
public class ModuleController {

    private final ModuleService moduleService;

    @GetMapping
    public ServiceResponse<List<Module>> findRootModulesWithChildModules() {
        List<Module> result = moduleService.findRootModulesWithChildModules();
        return ServiceResponse.ok(result, result.size());
    }

    @GetMapping("/{id}")
    public ServiceResponse<Module> findModuleWithChildModules(@PathVariable UUID id) {
        return ServiceResponse.ok(moduleService.findModuleById(id));
    }

    @PostMapping
    public ServiceResponse<Module> createRootModule(
            @Valid @RequestBody ModuleRequest request) {
        return ServiceResponse.ok(moduleService.createRootModule(request));
    }

    @PostMapping("/{id}")
    public ServiceResponse<Module> createChildModule(
            @PathVariable UUID id,
            @Valid @RequestBody ModuleRequest request) {
        return ServiceResponse.ok(moduleService.createChildModule(id, request));
    }

    @DeleteMapping("/{id}")
    public ServiceResponse<String> deleteModuleWithChildModules(@PathVariable UUID id) {
        moduleService.deleteModuleWithChildModules(id);
        return ServiceResponse.ok();
    }

}
