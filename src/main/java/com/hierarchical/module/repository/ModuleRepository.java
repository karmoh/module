package com.hierarchical.module.repository;

import com.hierarchical.module.entiy.Module;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ModuleRepository extends JpaRepository<Module, UUID> {
    List<Module> findByParentIsNull();
}
