package com.hierarchical.module.config.exception;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.lang.invoke.MethodHandles;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@ControllerAdvice
@AllArgsConstructor
public class ErrorControllerAdvice {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final Clock clock;

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorMessage> bindException(final MethodArgumentNotValidException e) {
        List<String> errors = new ArrayList<>();
        e.getBindingResult()
                .getAllErrors()
                .forEach(
                        fieldError -> {
                            FieldError error = (FieldError) fieldError;
                            errors.add(error.getField() + " " + error.getDefaultMessage());
                        });
        return error(e, ErrorCodes.INVALID_PARAMS, String.join(", ", errors), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorMessage> notFoundException(final NotFoundException e) {
        return error(e, ErrorCodes.MODULE_NOT_FOUND, e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MaximumDeptException.class)
    public ResponseEntity<ErrorMessage> maximumDeptException(final MaximumDeptException e) {
        return error(e, ErrorCodes.MAXIMUM_DEPT_EXCEEDED, e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessage> technicalException(final Exception exception) {
        return error(
                exception,
                ErrorCodes.TECHNICAL_EXCEPTION,
                "Technical exception",
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<ErrorMessage> error(
            final Exception exception, ErrorCodes errorCode, String message, HttpStatus httpStatus) {
        ErrorMessage responseMessage = ErrorMessage.builder()
                .ref(UUID.randomUUID())
                .errorCode(errorCode.getErrorCode())
                .message(message)
                .dateTime(LocalDateTime.now(clock))
                .build();
        LOGGER.error(responseMessage.toString(), exception);
        return new ResponseEntity<>(responseMessage, httpStatus);
    }
}
