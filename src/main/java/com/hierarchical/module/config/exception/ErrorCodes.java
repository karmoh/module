package com.hierarchical.module.config.exception;

public enum ErrorCodes {
    TECHNICAL_EXCEPTION,
    MODULE_NOT_FOUND,
    INVALID_PARAMS,
    MAXIMUM_DEPT_EXCEEDED;

    public String getErrorCode() {
        return this.name().toLowerCase();
    }
}
