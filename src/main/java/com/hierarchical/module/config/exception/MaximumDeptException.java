package com.hierarchical.module.config.exception;

public class MaximumDeptException extends RuntimeException {

    public MaximumDeptException(String message) {
        super(message);
    }
}
