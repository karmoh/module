package com.hierarchical.module.service;

import com.hierarchical.module.config.ApplicationProperties;
import com.hierarchical.module.config.exception.MaximumDeptException;
import com.hierarchical.module.config.exception.NotFoundException;
import com.hierarchical.module.dto.ModuleRequest;
import com.hierarchical.module.entiy.Module;
import com.hierarchical.module.repository.ModuleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class ModuleService {

    private final ModuleRepository moduleRepository;
    private final ApplicationProperties applicationProperties;

    public List<Module> findRootModulesWithChildModules() {
        return moduleRepository.findByParentIsNull();
    }

    public Module createRootModule(ModuleRequest request) {
        Module module = Module.builder()
                .name(request.getName())
                .build();
        moduleRepository.save(module);
        return module;
    }

    @Transactional
    public Module createChildModule(UUID parentId, ModuleRequest request) {
        Module parent = findModuleById(parentId);
        validateParentModuleDeptLevel(parent);
        Module module = Module.builder()
                .name(request.getName())
                .parent(parent)
                .build();
        moduleRepository.save(module);
        return module;
    }

    private void validateParentModuleDeptLevel(Module module) {
        int maximumDept = applicationProperties.getMaximumDept();
        int deptLevel = 1;
        Module parent = module.getParent();
        while (parent != null) {
            deptLevel++;
            if (deptLevel == maximumDept) {
                throw new MaximumDeptException("Module maximum dept of " + maximumDept + " exceeded");
            }
            parent = parent.getParent();
        }
    }


    public Module findModuleById(UUID id) {
        return moduleRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Module with id " + id + " not found"));
    }

    public void deleteModuleWithChildModules(UUID id) {
        Module module = findModuleById(id);
        moduleRepository.delete(module);
    }
}
