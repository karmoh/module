package com.hierarchical.module.service;

import com.hierarchical.module.config.ApplicationProperties;
import com.hierarchical.module.config.exception.MaximumDeptException;
import com.hierarchical.module.config.exception.NotFoundException;
import com.hierarchical.module.dto.ModuleRequest;
import com.hierarchical.module.entiy.Module;
import com.hierarchical.module.repository.ModuleRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ModuleServiceTest extends AbstractServiceTest<ModuleService> {

    @Mock
    private ModuleRepository moduleRepository;

    @Mock
    private Module module;

    @Test
    void findRootModulesWithChildModules() {
        when(moduleRepository.findByParentIsNull()).thenReturn(Collections.singletonList(module));
        List<Module> result = createService().findRootModulesWithChildModules();
        assertEquals(module, result.get(0));
    }

    @Test
    void createRootModule() {
        ModuleRequest request = ModuleRequest.builder().name("name").build();
        Module module = Module.builder()
                .name(request.getName())
                .build();
        createService().createRootModule(request);
        verify(moduleRepository).save(module);
    }

    @Test
    void createChildModule() {
        UUID parentId = UUID.randomUUID();
        ModuleRequest request = ModuleRequest.builder().name("name").build();
        when(moduleRepository.findById(parentId)).thenReturn(Optional.of(module));
        Module childModule = Module.builder()
                .name(request.getName())
                .parent(module)
                .build();
        createService().createChildModule(parentId, request);
        verify(moduleRepository).save(childModule);
    }

    @Test
    void createChildModuleModuleDeptExceeded() {
        UUID parentId = UUID.randomUUID();
        ModuleRequest request = ModuleRequest.builder().name("name").build();
        when(moduleRepository.findById(parentId)).thenReturn(Optional.of(module));
        when(module.getParent()).thenReturn(module);
        assertThrows(MaximumDeptException.class, () ->
                createService().createChildModule(parentId, request));
    }

    @Test
    void findModuleById() {
        UUID id = UUID.randomUUID();
        when(moduleRepository.findById(id)).thenReturn(Optional.of(module));
        Module result = createService().findModuleById(id);
        assertEquals(module, result);
    }

    @Test
    void findModuleByIdNotFound(){
        UUID id = UUID.randomUUID();
        when(moduleRepository.findById(id)).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () ->
                createService().findModuleById(id));
    }

    @Test
    void deleteModuleWithChildModules() {
        UUID id = UUID.randomUUID();
        when(moduleRepository.findById(id)).thenReturn(Optional.of(module));
        createService().deleteModuleWithChildModules(id);
        verify(moduleRepository).delete(module);
    }

    @Override
    protected ModuleService createService() {
        return new ModuleService(moduleRepository, new ApplicationProperties(2));
    }
}