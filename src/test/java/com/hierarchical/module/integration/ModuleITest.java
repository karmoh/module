package com.hierarchical.module.integration;

import com.hierarchical.module.dto.ModuleRequest;
import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.hamcrest.Matchers.equalTo;

class ModuleITest extends IntegrationTestBase {
    private static final String SERVICE_PATH = "modules";


    @Test
    void moduleFlow() {
        //create root module
        ModuleRequest request = ModuleRequest.builder().name("root").build();
        String rootId = RestAssuredMockMvc
                .given()
                .contentType(ContentType.JSON)
                .body(request)
                .when()
                .post(SERVICE_PATH)
                .then()
                .log().all()
                .statusCode(HttpStatus.OK.value())
                .body("data.name", equalTo("root")).extract().body().path("data.id");

        //create first child module
        request = ModuleRequest.builder().name("child1").build();
        String firstChildId = RestAssuredMockMvc
                .given()
                .contentType(ContentType.JSON)
                .body(request)
                .when()
                .post(SERVICE_PATH + "/{0}", rootId)
                .then()
                .log().all()
                .statusCode(HttpStatus.OK.value())
                .body("data.name", equalTo("child1")).extract().body().path("data.id");
        //create second child module
        request = ModuleRequest.builder().name("child2").build();
        String secondChildId = RestAssuredMockMvc
                .given()
                .contentType(ContentType.JSON)
                .body(request)
                .when()
                .post(SERVICE_PATH + "/{0}", firstChildId)
                .then()
                .log().all()
                .statusCode(HttpStatus.OK.value())
                .body("data.name", equalTo("child2")).extract().body().path("data.id");
        //try to create third child
        request = ModuleRequest.builder().name("child3").build();
        RestAssuredMockMvc
                .given()
                .contentType(ContentType.JSON)
                .body(request)
                .when()
                .post(SERVICE_PATH + "/{0}", secondChildId)
                .then()
                .log().all()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .body("errorCode", equalTo("maximum_dept_exceeded"))
                .body("message", equalTo("Module maximum dept of 3 exceeded"));
        //get all root objects
        RestAssuredMockMvc
                .given()
                .when()
                .get(SERVICE_PATH)
                .then()
                .log().all()
                .statusCode(HttpStatus.OK.value())
                .body("total", equalTo(1))
                .body("data[0].name", equalTo("root"))
                .body("data[0].id", equalTo(rootId))
                .body("data[0].childModules[0].name", equalTo("child1"))
                .body("data[0].childModules[0].id", equalTo(firstChildId))
                .body("data[0].childModules[0].childModules[0].name", equalTo("child2"))
                .body("data[0].childModules[0].childModules[0].id", equalTo(secondChildId));
        // delete root object
        RestAssuredMockMvc
                .given()
                .when()
                .delete(SERVICE_PATH + "/{0}", rootId)
                .then()
                .log().all()
                .statusCode(HttpStatus.OK.value());
        //find root module
        RestAssuredMockMvc
                .given()
                .when()
                .get(SERVICE_PATH + "/{0}", rootId)
                .then()
                .log().all()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .body("errorCode", equalTo("module_not_found"))
                .body("message", equalTo("Module with id " + rootId + " not found"));
        //find all root module
        RestAssuredMockMvc
                .given()
                .when()
                .get(SERVICE_PATH)
                .then()
                .log().all()
                .statusCode(HttpStatus.OK.value())
                .body("total", equalTo(0));
    }


}
