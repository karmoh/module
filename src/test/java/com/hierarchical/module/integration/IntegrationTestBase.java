package com.hierarchical.module.integration;

import com.hierarchical.module.ModuleApplication;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.context.WebApplicationContext;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ModuleApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class IntegrationTestBase {

    @Autowired
    private WebApplicationContext context;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        RestAssuredMockMvc.webAppContextSetup(context);
    }

    @AfterEach
    public void cleanUp() {
        RestAssuredMockMvc.reset();
    }


}
