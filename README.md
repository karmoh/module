# Hierarchical Modules

### Prerequisites

You will need Java 11

Maximum module dept can be configured in application.properties with

```
app.maximumDept=3
```

## Running application

```
gradlew bootRun
```

Go to [Swagger](http://localhost:8080/swagger-ui/) resources to get create and delete modules

## Running the tests

```
gradlew test
```

## Built With

* Java11
* Spring Boot
* H2 database
* Lombok
* SpringFox
* REST Assured
